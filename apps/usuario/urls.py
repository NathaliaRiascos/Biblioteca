from django.urls import path
from apps.usuario.views import ListadoUser, RegistrarUser
from django.contrib.auth.decorators import login_required

urlpatterns = [
  path('listado_usuarios/', login_required(ListadoUser.as_view()), name='listar_usuarios'),
  path('registrar_usuario/', login_required(RegistrarUser.as_view()), name='registrar_usuario')
]