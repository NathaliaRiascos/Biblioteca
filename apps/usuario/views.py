from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.contrib.auth import login, authenticate
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.generic.edit import FormView
from django.views.generic import CreateView, ListView, UpdateView, DeleteView
from apps.usuario.models import User
from .forms import FormularioLogin, FormUser

# Create your views here.

class Login(FormView):
    template_name= 'libro/login.html'
    form_class = FormularioLogin
    success_url = reverse_lazy('index')
    
    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    
    def dispatch(self,request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(self.get_success_url())
        else:
            return super(Login, self).dispatch(request,*args,**kwargs)
    
    def form_valid(self, form):
        login(self.request, form.get_user())
        return super(Login,self).form_valid(form)
    
class ListadoUser(ListView):
    model = User
    template_name = 'usuarios/listar_usuario.html'
    
    def get_queryset(self):
        return self.model.objects.filter(user_active = True)
    
class RegistrarUser(CreateView):
    model = User
    form_class = FormUser
    template_name = 'usuarios/crear_usuario.html'
    #success_url = reverse_lazy('usuarios:listar_usuarios')
    
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            nuevo_usuario = User(
                email = form.cleaned_data['email'],
                username = form.cleaned_data['username'],
                nombres = form.cleaned_data['nombres'],
                apellidos = form.cleaned_data['apellidos']
            )
            
            nuevo_usuario.set_password(form.cleaned_data['password1'])
            nuevo_usuario.save()
            return redirect('usuarios:listar_usuarios')
        else:
            return render(request, self.template_name, {'form': form})