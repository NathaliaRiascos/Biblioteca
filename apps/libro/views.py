from django.shortcuts import render, redirect
from django.core.exceptions import ObjectDoesNotExist
from django.views.generic import TemplateView, View, ListView, UpdateView, CreateView, DeleteView
from django.urls import reverse_lazy
from .forms import AutorForm, LibroForm
from .models import *

'''
    1. dispatch(): Valida la peticion y elige que metodo HTTP se utilizo para la solicitud
    2. http_method_not_allowes(): Retorna un error cuando se utiliza un metodo http no soportado o definido
    3. coptions() 
'''
# BASADA EN CLASES

class Inicio(TemplateView):
    template_name = 'libro/index.html'
    

class ListadoAutor(ListView):
    model = Autor
    template_name = 'libro/autor/listar_autor.html'
    context_object_name = 'autores' #Defecto = object_list
    queryset = Autor.objects.filter(estado = True)

class ActualizarAutor(UpdateView):
    model = Autor
    template_name = 'libro/autor/crear_autor.html'
    form_class = AutorForm
    success_url = reverse_lazy('libro:listar_autor')

class CrearAutor(CreateView):
    model =  Autor
    form_class = AutorForm
    template_name = 'libro/autor/crear_autor.html'
    success_url = reverse_lazy('libro:listar_autor')

class EliminarAutor(DeleteView):
    model=  Autor
    
    def post(self, request, pk, *args, **kwargs):
        autor = Autor.objects.get(id = pk)
        autor.estado = False
        autor.save()
        return redirect('libro:listar_autor')


class ListadoLibros(View):
    model = Libro
    form_class = LibroForm
    template_name = 'libro/libro/listar_libro.html'
    
    
    def get_queryset(self):
        return self.model.objects.filter(estado = True)
    
    def get_context_data(self, **kwargs):
        context = {}
        context['libros'] = self.get_queryset()
        context['form'] = self.form_class
        return context
        
    def get(self, request, *args, **kwargs):
        context = {'libros': self.get_queryset()}
        return render(request, self.template_name, self.get_context_data())
    
   
  
class CrearLibro(CreateView):
    model = Libro
    form_class = LibroForm 
    template_name = 'libro/libro/crear_libro.html'
    success_url = reverse_lazy('libro:listar_libro')
    

class ActualizarLibro(UpdateView):
    model = Libro
    template_name = 'libro/libro/libro.html'
    form_class = LibroForm
    success_url = reverse_lazy('libro:listar_libro')
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['libros'] = Libro.objects.filter(estado = True)
        return context

class EliminarLibro(DeleteView):
    model=  Libro
    
    def post(self, request, pk, *args, **kwargs):
        libro = Libro.objects.get(id = pk)
        libro.estado = False
        libro.save()
        return redirect('libro:listar_libro')
